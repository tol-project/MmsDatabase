------------------------------------------------------------------------------
-- Tablas para almacenamento de parámetros estimados de MMS
------------------------------------------------------------------------------

-- INSERT INTO mms_d_module
-- VALUES ('PARAMETERS', 1); 

-- DROP TABLE mms_f_parameter_r; 
------------------------------------------------------------------------------
CREATE TABLE mms_f_parameter_r (
------------------------------------------------------------------------------
  id_object     INTEGER  NOT NULL,
  co_parameter  VARCHAR(1024) NOT NULL,
  co_node       VARCHAR(1024) NOT NULL,
  co_term       VARCHAR(1024),
  co_type       VARCHAR(1024)  NOT NULL,
  nu_block      INTEGER, -- period, category, piece
  nu_degree     INTEGER,
  -- definition
  vl_initial      NUMERIC,
  vl_is_fixed     NUMERIC,
  vl_prior_mean   NUMERIC,
  vl_prior_sigma  NUMERIC,
  vl_cns_minimum  NUMERIC,
  vl_cns_maximum  NUMERIC,
  -- results
  vl_mean     NUMERIC,
  vl_sigma    NUMERIC,
  vl_minimum  NUMERIC,
  vl_maximum  NUMERIC,
  vl_median   NUMERIC,
  CONSTRAINT pk1_mms_f_parameter_r
    PRIMARY KEY (id_object, co_parameter),
  CONSTRAINT fk1_mms_f_parameter_r
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);
	
-- DROP TABLE mms_f_mcombination_r; 
------------------------------------------------------------------------------	
CREATE TABLE mms_f_mcombination_r (
------------------------------------------------------------------------------
  id_object       INTEGER  NOT NULL,
  co_combination  VARCHAR(1024)  NOT NULL,
  -- definition
  vl_initial      NUMERIC,
  vl_prior_mean   NUMERIC,
  vl_prior_sigma  NUMERIC,
  vl_cns_minimum  NUMERIC,
  vl_cns_maximum  NUMERIC,
  -- results
  vl_mean     NUMERIC,
  vl_sigma    NUMERIC,
  vl_minimum  NUMERIC,
  vl_maximum  NUMERIC,
  vl_median   NUMERIC,
  CONSTRAINT pk1_mms_f_mcombination_r
    PRIMARY KEY (id_object, co_combination),
  CONSTRAINT fk1_mms_f_mcombination_r
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);	

------------------------------------------------------------------------------
-- Tablas para almacenamento de resultados de MMS
------------------------------------------------------------------------------

INSERT INTO mms_d_module
VALUES ('RESULTS', 1); 

-- DROP TABLE mms_d_submodel_r; 
------------------------------------------------------------------------------
CREATE TABLE mms_d_submodel_r (
------------------------------------------------------------------------------
  id_object           INTEGER  NOT NULL,
  co_submodel         VARCHAR  NOT NULL,
  co_type             VARCHAR  NOT NULL,
  co_function         VARCHAR,  -- link|transformation  ej: LOG PROBIT
  co_arima_label      VARCHAR,  -- ej: P1DIF1AR1MA1
  vl_data_size        NUMERIC,
  vl_parameters_size  NUMERIC,
  vl_log_likelihood   NUMERIC,
  CONSTRAINT pk1_mms_d_submodel_r
    PRIMARY KEY (id_object, co_submodel),
  CONSTRAINT fk1_mms_d_submodel_r
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object) 
);

-- DROP TABLE mms_f_submodel_r_statistic; 
------------------------------------------------------------------------------
CREATE TABLE mms_f_submodel_r_statistic (
------------------------------------------------------------------------------
  id_object     INTEGER  NOT NULL,
  co_submodel   VARCHAR  NOT NULL,
  co_statistic  VARCHAR  NOT NULL,
  vl_statistic  NUMERIC  NOT NULL,
  CONSTRAINT pk1_mms_f_submodel_r_statistic
    PRIMARY KEY (id_object, co_submodel, co_statistic),
  CONSTRAINT fk1_mms_f_submodel_r_statistic
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);

-- DROP TABLE mms_f_submodelc_r_series; 
------------------------------------------------------------------------------
CREATE TABLE mms_f_submodelc_r_series (
------------------------------------------------------------------------------
  id_object           INTEGER  NOT NULL,
  co_submodel         VARCHAR,
  dt_series           TIMESTAMP,
  vl_observations     NUMERIC,
  vl_output           NUMERIC,
  vl_interruptions    NUMERIC,
  vl_additive_filter  NUMERIC,
  vl_filter           NUMERIC,
  vl_noise            NUMERIC, 
  vl_dif_noise        NUMERIC,
  vl_residuals        NUMERIC,
  vl_std_residuals    NUMERIC,
  vl_prediction       NUMERIC, 
  vl_obs_prediction   NUMERIC,
  CONSTRAINT pk1_mms_f_submodelc_r_series
    PRIMARY KEY (id_object, co_submodel, dt_series),
  CONSTRAINT fk1_mms_f_submodelc_r_series
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);

-- DROP TABLE mms_f_submodeld_r_matrix; 
------------------------------------------------------------------------------
CREATE TABLE mms_f_submodeld_r_matrix (
------------------------------------------------------------------------------
  id_object      INTEGER  NOT NULL,
  co_submodel    VARCHAR,
  nu_row         INTEGER,
  vl_output      NUMERIC,
  vl_filter      NUMERIC,
  vl_residuals   NUMERIC,
  vl_prediction  NUMERIC, 
  CONSTRAINT pk1_mms_f_submodeld_r_matrix
    PRIMARY KEY (id_object, co_submodel, nu_row),
  CONSTRAINT fk1_mms_f_submodeld_r_matrix
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);
	
-- DROP TABLE mms_f_submodelc_f_series; 
------------------------------------------------------------------------------
CREATE TABLE mms_f_submodelc_f_series (
------------------------------------------------------------------------------
  id_object           INTEGER    NOT NULL,
  co_submodel         VARCHAR    NOT NULL,
  dt_series           TIMESTAMP  NOT NULL,
  vl_obs_orig         NUMERIC,
  vl_obs_mean         NUMERIC,
  vl_obs_median       NUMERIC,
  vl_obs_lower        NUMERIC,
  vl_obs_upper        NUMERIC, 
  vl_output           NUMERIC,
  vl_output_sigma     NUMERIC,
  vl_additive_filter  NUMERIC,
  vl_filter           NUMERIC,
  vl_noise            NUMERIC,
  CONSTRAINT pk1_mms_f_submodelc_f_series
    PRIMARY KEY (id_object, co_submodel, dt_series),
  CONSTRAINT fk1_mms_f_submodelc_f_series
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);	

------------------------------------------------------------------------------
-- Tablas para almacenamento de los res�menes de los objetos MMS
------------------------------------------------------------------------------

INSERT INTO mms_d_module
VALUES ('SUMMARY', 1); 

-- DROP TABLE mms_d_object_summary; 
------------------------------------------------------------------------------
CREATE TABLE mms_d_object_summary (
------------------------------------------------------------------------------
  id_object   INTEGER  NOT NULL,
  co_section  VARCHAR  NOT NULL,
  co_name     VARCHAR  NOT NULL,
  co_grammar  VARCHAR  NOT NULL, 
  co_value    VARCHAR  NOT NULL,
  CONSTRAINT pk1_mms_d_object_summary
    PRIMARY KEY (id_object, co_section, co_name),
  CONSTRAINT fk1_mms_d_object_summary
    FOREIGN KEY (id_object) REFERENCES mms_d_object (id_object)
);

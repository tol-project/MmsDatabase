------------------------------------------------------------------------------
-- Tablas b�sicas para el almacenamiento de objetos de MMS
------------------------------------------------------------------------------

INSERT INTO mms_d_module
VALUES ('BASE', 1); 

-- DROP TABLE mms_d_object; 
------------------------------------------------------------------------------
CREATE TABLE mms_d_object (
------------------------------------------------------------------------------
  id_object        SERIAL     NOT NULL,
  co_subclass      VARCHAR    NOT NULL,
  co_name          VARCHAR    NOT NULL,
  co_version       VARCHAR    NOT NULL,
  ds_object        VARCHAR,
  ds_tags          VARCHAR, 
  dt_creation      TIMESTAMP  NOT NULL,
  dt_modification  TIMESTAMP  NOT NULL,
  CONSTRAINT pk1_mms_d_object
    PRIMARY KEY (id_object),
  CONSTRAINT pk2_mms_d_object
    UNIQUE (co_subclass, co_name, co_version)
); 	

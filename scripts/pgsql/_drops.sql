
DROP TABLE IF EXISTS mms_d_submodel_r; 
DROP TABLE IF EXISTS mms_f_submodel_r_statistic; 
DROP TABLE IF EXISTS mms_f_submodelc_r_series; 
DROP TABLE IF EXISTS mms_f_submodelc_r_matrix; 
DROP TABLE IF EXISTS mms_f_submodeld_r_matrix; 
DROP TABLE IF EXISTS mms_f_submodelc_f_series; 

DROP TABLE IF EXISTS mms_f_parameter_r; 
DROP TABLE IF EXISTS mms_f_mcombination_r; 

DROP TABLE IF EXISTS mms_d_object_oza; 
DROP TABLE IF EXISTS mms_d_object_summary; 
DROP TABLE IF EXISTS mms_d_object; 
DROP TABLE IF EXISTS mms_d_module;
